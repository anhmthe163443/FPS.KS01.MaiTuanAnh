﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.Company
{
    public class CompanyModel:BaseModel
    {
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set; }
    }
}
