﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository: BaseRepository, ICompanyRepository
    {
        public CompanyRepository(FPSContext context):base(context) 
        {
        }
        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company= new FPS.DataLayer.Entity.Company();
            company.Id=Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreatedDate=DateTime.Now;
            company.UpdatedDate=DateTime.Now;
            company.CreatedBy = model.CreatedBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted=false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public void DeleteCompany(Guid companyId)
        {
            var companyToDelete = _context.Companies.FirstOrDefault(c => c.Id == companyId);

            if (companyToDelete != null) 
            {
                _context.Companies.Remove(companyToDelete);
                _context.SaveChanges();
            }
        }
        public void UpdateCompany(Guid companyId, CompanyModel model)
        {
            var companyToUpdate = _context.Companies.FirstOrDefault(c => c.Id == companyId);

            if (companyToUpdate != null)
            {
                
                companyToUpdate.CompanyName = model.CompanyName;
                companyToUpdate.UpdatedDate = DateTime.Now;
                companyToUpdate.UpdatedBy = model.UpdatedBy;

                _context.SaveChanges();
            }
        }
        public List<CompanyModel> GetAllCompany()
        {
            var companies = _context.Companies.ToList();

            return companies.Select(c => new CompanyModel
            {
                CompanyName = c.CompanyName,
                UpdatedDate = c.UpdatedDate,
                UpdatedBy = c.UpdatedBy
            }).ToList();
        }


    }
}
