﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;
        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }
        [HttpPost]
        [Route("Addcompany")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }


        [HttpDelete]
        [Route("DeleteCompany")]
        public void DeleteCompany(CompanyModel model)
        {
            _companyRepository.DeleteCompany(model.Id);
        }

        [HttpPut]
        [Route("UpdateCompany")]
        public void UpdateCompany(Guid companyId, CompanyModel model)
        {
            _companyRepository.UpdateCompany(companyId, model);
        }
        [HttpGet]
        [Route("GetAllCompany")]
        public List<CompanyModel>GetAllCompany()
        {
             return _companyRepository.GetAllCompany();
        }


    }
}
