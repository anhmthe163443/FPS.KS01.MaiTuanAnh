﻿using FPS.BusinessLayer.ViewModel.Company;

namespace FPS.MVC.Serveice.Company
{
    public class CompanyService:BaseService,   ICompanyService
    {

        public const string BasePath = "/api/Company/GetAllCompany";

        public CompanyService(HttpClient client) :base(client)
        
        {
        
        } 

        public List<CompanyViewModel> GetAllCompany()
        {
            var response = _client.GetAsync(BasePath);
            return null;
        }
    }
}
