﻿using FPS.BusinessLayer.ViewModel.Company;

namespace FPS.MVC.Serveice.Company
{
    public interface ICompanyService
    {
        public List<CompanyViewModel> GetAllCompany();
    }
}
