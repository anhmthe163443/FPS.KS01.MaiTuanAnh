﻿using FPS.MVC.Serveice.Company;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public IActionResult Index()
        {
            var test = _companyService.GetAllCompany();
            return View();
        }
    }
}
