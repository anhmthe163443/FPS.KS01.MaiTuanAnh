﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class User:IdentityUser
    {
        public string FullName { get; set; } = null!;
        public DateTime DOB { get; set; }
        public string? Address { get; set; }
        public bool Status { get; set; }

    }
}
